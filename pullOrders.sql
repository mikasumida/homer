homer_productions
WbExport -file='Desktop/prodOrders.txt'
          -type=text
          -delimiter='\t'
          -dateFormat='yyyy-MM-dd';
select id as orderID
    , restaurant_id
    , restaurant_name
    , (created_at::timestamp at time zone 'UTC' at time zone 'America/New_York') as created_at
, (submitted_at::timestamp at time zone 'UTC' at time zone 'America/New_York') as submitted_at
    , product_total
    , order_type
from orders ord
where rider_id is not null
and pickedup_at is not null
and submitted_at is not null
and created_at is not null
and deleted_at is null
and canceled_at is null
and status not in ('NEED VALIDATION', 'OUT_OF_DELIVERY_ZONE')
and (created_at::timestamp at time zone 'UTC' at time zone 'America/New_York') between '20160401' and '20160630'
order by created_at;


merch orders
WbExport -file='Desktop/merchOrders.txt'
          -type=text
          -delimiter='\t'
          -dateFormat='yyyy-MM-dd';
select del.tracking_code as prodOrderID
        , ord.id as merchOrderID
        , ch.name as channelName
        , (ord.created_at::timestamp at time zone 'UTC' at time zone 'America/New_York') as created_at
        , ord.external_group_id
        , ord.orders_count
        , sum(grps.items_count) as groupItemCount
        , ord.items_count
        , ord.subtotal_cents/100 as subtotal
        , ord.items_json
from orders ord
inner join deliveries del
on del.order_id = ord.id
left join channels ch
on ord.channel_id = ch.id
left join orders grps
on ord.id = grps.group_order_id
where ord.service_type = 'delivery'
and (ord.created_at::timestamp at time zone 'UTC' at time zone 'America/New_York') between '20160331' and '20160701'
and del.tracking_code is not null
group by 1, 2, 3, 4, 5, 6, 8, 9, 10
order by created_at;

WbExport -file='Desktop/prodOrders.csv'
          -type=text
          -delimiter='\t';
select ord.*
from orders ord
where rider_id is not null
    and pickedup_at is not null
    and submitted_at is not null
    and created_at is not null
    and deleted_at is null
    and canceled_at is null
    and status not in ('NEED VALIDATION', 'OUT_OF_DELIVERY_ZONE')
    and (created_at::timestamp at time zone 'UTC' at time zone 'America/New_York') between '20160401' and '20160731'
order by created_at;

WbExport -file='Desktop/merchOrders.csv'
          -type=text
          -delimiter='\t';
select distinct del.tracking_code as prodOrderID
        , ch.name as channelName
        , sum(grps.items_count) over (partition by del.tracking_code) as groupItemCount
        , max(items_json) as groupItem
        , ord.*
from orders ord
inner join deliveries del
    on del.order_id = ord.id
left join channels ch
    on ord.channel_id = ch.id
left join orders grps
    on ord.id = grps.group_order_id
where ord.service_type = 'delivery'
    and (ord.created_at::timestamp at time zone 'UTC' at time zone 'America/New_York') between '20160331' and '20160801'
    and del.tracking_code is not null
order by created_at;





awk 'FNR==NR{a[$1]=$0;FS="\t";next}{ print $0 FS a[$1]}' merchOrders.csv prodOrders.csv > joinedOrders.csv
