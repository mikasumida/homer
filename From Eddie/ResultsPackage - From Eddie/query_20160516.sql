select 
o.id,o.restaurant_id,o.recipient_id,z.name as zone,
(o.submitted_at::timestamp at time zone 'UTC' at time zone 'America/New_York') as submitted_at,
(o.pickup_arrival_at::timestamp at time zone 'UTC' at time zone 'America/New_York') as pickup_arrival_at,
(o.pickedup_at::timestamp at time zone 'UTC' at time zone 'America/New_York') as pickedup_at,
(o.arrival_destination_at::timestamp at time zone 'UTC' at time zone 'America/New_York') as arrival_destination_at,
(o.delivered_at::timestamp at time zone 'UTC' at time zone 'America/New_York') as delivered_at,
o.origin_lat,o.origin_lon,o.destination_lat,o.destination_lon,
o.order_type,o.product_total
from orders o
inner join restaurants r on r.id = o.restaurant_id
inner join zones z on z.id = r.operating_zone_id
where (o.submitted_at::timestamp at time zone 'UTC' at time zone 'America/New_York') >= '2016-05-01'
and (o.submitted_at::timestamp at time zone 'UTC' at time zone 'America/New_York') < '2016-05-20'
--where (o.submitted_at::timestamp at time zone 'UTC' at time zone 'America/New_York') >= '2016-05-01'
and o.status IN ('DELIVERED','NOT-FULFILLED','UNDELIVERABLE')
and o.deleted_at is null
and o.submitted_at is not null
and o.rider_id is not null
and o.pickedup_at is not null
and o.created_at is not null
and z.name IN ('MTW','CHEL','MTE')
