#! /usr/bin/julia
using DataFrames;

# Function to calcuate if two orders both lie within a maximum circle distance in both restaurant location (r) and customer location (c)
# Returns true if within maximum distance, false otherwise
function cosDistanceBundle(lat1r, lon1r, lat2r, lon2r, lat1c, lon1c, lat2c, lon2c, maxdist)
	c = pi/180;
	
	if ( ((abs(lat1r - lat2r) < 0.0000000001) && (abs(lon1r - lon2r) < 0.0000000001))  || 
		(acos(sin(lat1r*c) * sin(lat2r*c) + cos(lat1r*c) * cos(lat2r*c) * cos(lon2r*c-lon1r*c)) * 6371000 < maxdist))
		if ( (abs(lat1c - lat2c) < 0.0000000001) && (abs(lon1c - lon2c) < 0.0000000001))
			return true;
		elseif (acos(sin(lat1c*c) * sin(lat2c*c) + cos(lat1c*c) * cos(lat2c*c) * cos(lon2c*c-lon1c*c)) * 6371000 < maxdist)
			return true;
		end
	end
	return false;
end


#= What this simulation does is:
  1. Sort orders by ready-for-pickup time
  2. Iterate through orders.
  3. From the 'anchor' order, look forward at all orders that become ready for pickup within policy minutes, and see if they are eligible for bundling.
     Policy minutes is the number of minutes we are willing to hold an order.  So we can look at all ord
  4. An assumption here is that we always bundle orders with (i.e. no order is never held back in anticipation of a 'better' bundle)
=#


# Cases for comparison/simulation 
MAXBUNDLESIZEVEC = [2,3,4]
MAXMETERDISTVEC = [300,345,400]
MAXWAITTIMEVEC = [10,15,20]

# Iterate through different cases
for bundle_iter in MAXBUNDLESIZEVEC
for meter_iter in MAXMETERDISTVEC
for waittime_iter in MAXWAITTIMEVEC

#MAX_WAIT_AFTER_READY_MIN = 20;
#MAX_BUNDLE_DIST_METER = 365;
#MAX_BUNDLE_SIZE = 3;

MAX_WAIT_AFTER_READY_MIN = waittime_iter;
MAX_BUNDLE_DIST_METER = meter_iter;
MAX_BUNDLE_SIZE = bundle_iter;

print("Starting Iteration: maxSz$(bundle_iter) maxMt$(meter_iter) maxWait$(waittime_iter)");


#orders = readtable("/Users/eddieelizondo/Dropbox/_homer/_3.orderqueue/orderdata_20160521.csv");
orders = readtable("/root/orderdata_20160521.csv");
dateformat = Dates.DateFormat("y-m-d H:M:S");
dateformat_ymd = Dates.DateFormat("yyyy-mm-dd");

orders[:submitted_at] = DateTime(orders[:submitted_at],dateformat);
#orders[:pickup_arrival_at] = DateTime(orders[:pickup_arrival_at],dateformat);
#orders[:pickedup_at] = DateTime(orders[:pickedup_at],dateformat);
#orders[:arrival_destination_at] = DateTime(orders[:arrival_destination_at],dateformat);
#orders[:delivered_at] = DateTime(orders[:delivered_at],dateformat);
orders[:date_string] = map(string,Date(orders[:submitted_at]));
sort!(orders,cols=[:submitted_at]);

# Indicator for whether order has been delivered in simulation
orders[:delivered] = 0;
orders[:bundle_id] = 0;
orders = orders[orders[:submitted_at] .> Date(2016,2,10),:];
gc();

# Get set of unique dates in dataset
deliveryDates = [k for k in unique(orders[:date_string])]

# Create output table
# Each row is a historical date.  Each column is the number of deliveries made of bundle size x
bundleResults = DataFrame(DeliveryDate = deliveryDates);
for i = 1:MAX_BUNDLE_SIZE
	bundleResults[symbol("DeliverySize_$i")] = zeros(length(deliveryDates));
end

# Subset orders data
ordersSim = orders;
ordersSim = ordersSim[[:id,:zone,:submitted_at,:origin_lat,:origin_lon,:destination_lat,:destination_lon,:date_string,:delivered,:bundle_id]];
ordersSim[:rowNum] = 1:size(ordersSim,1);
dailyDeliveries = Dict{ASCIIString,Dict{Any}}();

# Iterate through each row in orders table
for i = 1:size(ordersSim,1)
	
	# Move to next order if this one has already been delivered (through another bundle)
	if ordersSim[i,:delivered] == 1
		continue;
	end
	
	# Get future orders that are eligible for bundling, based on timing
	# These orders meet the following conditions:
	# * Order was ready for pickup in the future and within the maximum waiting time
	# * Order was in the same zone
	# * Order was not already delivered / part of an earlier bundle
	orderssub = ordersSim[(ordersSim[:rowNum] .> i) & (ordersSim[i,:submitted_at] + Dates.Minute(MAX_WAIT_AFTER_READY_MIN) .>= ordersSim[:submitted_at]) & (ordersSim[i,:zone] .== ordersSim[:zone]) & (ordersSim[:delivered] .== 0),:];

	# Create an array for all order ids that are eligible to be bundled
	bundleidarray = [ordersSim[i,:id]];
	
	# From subset of time-eligible orders, check if they are eligible to be bundled based on origin/delivery address
	for j = 1:size(orderssub,1)
		
		# Check if this order is within maximum circle distance on both restaurant and delivery of anchor order
		canbundle = cosDistanceBundle(ordersSim[i,:origin_lat], ordersSim[i,:origin_lon],orderssub[j,:origin_lat], orderssub[j,:origin_lon],
			ordersSim[i,:destination_lat], ordersSim[i,:destination_lon],orderssub[j,:destination_lat], orderssub[j,:destination_lon],
				MAX_BUNDLE_DIST_METER);
		if !canbundle
			continue;
		end
		push!(bundleidarray,orderssub[j,:id]);
	end
	a = orderssub;
	
	# Now create/log the bundles
	# If length(bundleidarray) > 2, then there is more than 1 order in this delivery (so a bundle)
	if length(bundleidarray) > 2
		
		# Each of the order IDs in bundleidarray are bundle-eligible with the anchor bundle.  
		# But they may not be bundle-eligible with each other are not eligible with the 
		# This loop goes through each order to make sure it is bundle-eligible with each other order in the bundle we're constructing
		
		# Create identity of matrix of size = length of bundleidarray
		# Use this matrix to store whether order is eligible with each other order...correspoding entry in matrix is 1
		bundleEligible = eye(length(bundleidarray));
		
		# We know the first order is bundle-eligible with every order, so make all values in that row to 1
		bundleEligible[1,:] = 1;
		for r = 2:(length(bundleidarray)-1)
			
			# Find row index in the orders table of this new "anchor" order id to get lat/lon data.  
			rInd = find(a[:id] .== bundleidarray[r])[1];

			# Check this order against other orders in bundleidarray
			for c = (r+1):length(bundleidarray)

				# Find row index in the orders table of this second order id to get lat/lon data
				cInd = find(a[:id] .== bundleidarray[c])[1];
			
				# Check if these 2 orders can be bundled
				bundleEligible[r,c] = (cosDistanceBundle(a[rInd,:origin_lat], a[rInd,:origin_lon],a[cInd,:origin_lat], a[cInd,:origin_lon],a[rInd,:destination_lat], a[rInd,:destination_lon],a[cInd,:destination_lat], a[cInd,:destination_lon],MAX_BUNDLE_DIST_METER)) * 1;
			end
		end
					
		numover = length(bundleidarray) - MAX_BUNDLE_SIZE;
		
		# Find number of orders each column (order) can be bundled with
		bundleMemberCt = sum(bundleEligible,1);
		
		# Cap by maximum bundle size
		bundleMemberCt[bundleMemberCt .> MAX_BUNDLE_SIZE] = MAX_BUNDLE_SIZE;
		
		# Get the size of the largest bundle (the bundle we will actually construct)
		actualbundlesize = maximum(bundleMemberCt);
		
		# Find the first column index with this bundle size (this will give us the earliest combination of orders reaching this bundle size)
		bundleIndex = findfirst(bundleMemberCt,actualbundlesize);
		
		# Get order ids that will be in the bundle
		bundleidarray_new = Int64[];
		for ind = 1:length(bundleidarray)
			if bundleEligible[ind,bundleIndex] == 1
				push!(bundleidarray_new,bundleidarray[ind]);
			end
			
			if (length(bundleidarray_new) == actualbundlesize)
				break
			end
		end
		bundleidarray = bundleidarray_new
	end

	# Mark bundled orders as "delivered"
	for j in bundleidarray
		ordersSim[ordersSim[:id] .== j,:delivered] = 1;
	end

	# Save bundle size into results table for this date
	bundleResults[bundleResults[:DeliveryDate] .== ordersSim[i,:date_string],symbol("DeliverySize_$(length(bundleidarray))")] += 1;

	#= if we want to keep history of bundles
		# make sure to add index to bundlearray	
		if !haskey(dailyDeliveries,ordersSim[i,:date_string])
			dailyDeliveries[ordersSim[i,:date_string]] = Dict();
			for j in 1:MAX_BUNDLE_SIZE
				dailyDeliveries[ordersSim[i,:date_string]][j] = Set{Array{Int64,1}}();
			end
			#print("Started day: $(ordersSim[i,:date_string]) \n");
		end
		push!(dailyDeliveries[ordersSim[i,:date_string]][length(bundleidarray)],bundleidarray);
	=#	

	# Force garbage collection because server might be memory constrained
	if i % 10000 == 0
		gc();
	end
end

# Write results to table
writetable("/root/bunResult_maxSz$(bundle_iter)_maxMt$(meter_iter)_maxWait$(waittime_iter).csv",bundleResults,separator=',',header=true);
print("Finished Iteration: maxSz$(bundle_iter) maxMt$(meter_iter) maxWait$(waittime_iter)");
end
end
end


# Deprecated distance functions
#function havDistance(lat1, lon1, lat2, lon2)
#	b = pi/180;
#	a = 0.5 - cos((lat2 - lat1) * b)/2 + cos(lat1 * b) * cos(lat2 * b) * (1 - cos((lon2 - lon1) * b)) / 2
#	return 12742000 * asin(sqrt(a));
#end
#
#function cosDistance(lat1, lon1, lat2, lon2)
#	c = pi/180;
#	acos(sin(lat1*c) * sin(lat2*c) + cos(lat1*c) * cos(lat2*c) * cos(lon2*c-lon1*c)) * 6371000
#end


